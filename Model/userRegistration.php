<?php 

include_once "../../DataBase/Dbh.php";

class userRegistration extends Dbh{
    private $userName;
    private $userPassword;
    private $userMail;

    public function __construct($userName,$userPassword,$userMail){
        $this->userName = $userName;
        $this->userPassword = $userPassword;
        $this->userMail = $userMail;
    }

    public function setName($userName){
        $this->userName = $userName;
    }

    public function setPassword($userPassword){
        $this->userPassword = $userPassword;
    }

    public function setMail($userMail){
        $this->userMail = $userMail;
    }

    public function getName(){
        return $this->userName;
    }

    public function getPassword(){
        return $this->userPassword;
    }

    public function getMail(){
        return $this->userMail;
    }
}