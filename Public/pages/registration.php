<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Registration Page</title>
    <link rel="stylesheet" href="../assets/css/registration.css">
    <script src="../assets/js/registration.js"></script>
</head>
<body>
<div class="container " id="container">
	<div class="form-container sign-up-container">
		<form action="../../View/registration/signUp.php" method="POST">
			<h1>Create Account</h1>
			<input type="text" name="regName" placeholder=": Enter UserName"/>
			<input type="text" name="regMail" placeholder=": Enter E-mail"/>
			<input type="password" name="regPassword" placeholder=": Enter Password" />
            <input type="password" name="rePassword" placeholder=": Confirm Password" />
			<button type="submit" name="signUp">Sign Up</button>
		</form>
	</div>
	<div class="form-container sign-in-container">
		<form action="../../View/registration/signIn.php" method="POST">
			<h1>Sign in</h1>
			<input type="text" name="logUser" placeholder=": Enter Email or UserName" />
			<input type="password" name="logPassword" placeholder=":Enter Password" />
			<a href="#">Forgot your password?</a>
			<button type="submit" name="signIn">Sign In</button>
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>Welcome Back!</h1>
				<p>To keep connected with us please login with your personal info</p>
				<button class="ghost" id="signIn" onclick='signIn()'>Sign In</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>Hello, Friend!</h1>
				<p>Enter your personal details and start journey with us</p>
				<button class="ghost" id="signUp" onclick='signUp()'>Sign Up</button>
			</div>
		</div>
	</div>
</div>
</body>
</html>