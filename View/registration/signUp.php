<?php 

include_once "../../Controller/signUpController.php";
    
    $name = $_POST['regName'];
    $mail = $_POST['regMail'];
    $password = $_POST['regPassword'];
    $confirm = $_POST['rePassword'];

    if(empty($name)||empty($password)||empty($mail)||empty($confirm)){
        header("Location: ../../Public/pages/registration.php?add=empty");
    }
    else{
        if($password == $confirm){
            $hashed_password = password_hash($password, PASSWORD_DEFAULT);
            signUpController::addUser($name,$hashed_password,$mail);
            header("Location: ../../Public/pages/registration.php?add=success");
        }
        else{
            header("Location: ../../Public/pages/registration.php?match=error");
        }
        
    }
}
else{
        header("Location: ../../Public/pages/registration.php?add=error");
}
