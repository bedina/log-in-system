<?php

if(isset($_POST['signIn'])){
    include_once '../../Controller/signInController.php';

    $userLog = $_POST['logUser'];
    $password = $_POST['logPassword'];

    if(empty($userLog)||empty($password)){
        header("Location: ../../Public/pages/registration.php?log=empty");
    }
    else{
        signInController::signUser($userLog,$password);
        /*if(signInController::signUser($userLog,$password)){
            header("Location: ../../Public/pages/loggedIn.php");
        }
        else{
            header("Location: ../../Public/pages/registration.php?log=incorrect");
        }*/
    }
}

else{
    header("Location: ../../Public/pages/registration.php?log=error");
}